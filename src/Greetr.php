<?php


namespace Inovcorp\Greetr;


class Greetr
{
    public function greet(String $sName)
    {
        return "Hi {$sName}! How are you doing today?";
    }

    public function goodbye(string $sName)
    {
        return "Goodbye {$sName}...";
    }

    public function cenas(string $sName)
    {
        return "Cenas {$sName}...";
    }
}
